# viupet_api

## Getting started

- From a push to the repository
- Open VScode in project folder
- Run the following command **npm i**
- After it installs the packages, run the command  **npm start**



## cd existing_repo
- git remote add origin https://gitlab.com/senac-projetos-de-desenvolvimento/2022-ramon-rocha/viupet_api.git
- git branch -M main
- git push -u origin main

## Name
viupet

## Description
This is a cbt project for college my project has the purpose of helping tutors to find lost animals and also to help vulnerable animals that live on the streets to find homes where they can have a family I believe this is a very important social problem for all.
